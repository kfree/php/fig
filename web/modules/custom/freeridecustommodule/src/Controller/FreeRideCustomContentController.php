<?php

namespace Drupal\FreeRideCustomContent\Controller;

use Drupal\Core\Controller\ControllerBase;

class FreeRideCustomContentController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => $this->t('Welcome to custom controller!'),
    );
  }

}