<?php
declare(strict_types=1);

namespace Drupal\FreeRideCustomContent;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface SiteMapEntityInterface extends ConfigEntityInterface {

  /**
   * @return integer
   */
  public function getIdentifier();

  /**
   * @return string
   */
  public function getTitle();

  /**
   * @return string
   */
  public function getAddress();

  /**
   * @return \DateTime
   */
  public function getLastUpdated();
}